# EKS-BASE MODULE

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| cluster\_version | EKS cluster version | `string` | `"1.12"` | no |
| default\_users | Additional IAM users to add to the aws-auth configmap. | <pre>list(object({<br>    userarn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | `[]` | no |
| efs\_subnets | List of IDs of EFS subnets | `list` | n/a | yes |
| eks\_cluster\_name | Name of EKS cluster | `string` | `"eks"` | no |
| map\_roles | Additional IAM roles to add to the aws-auth configmap. See examples/basic/variables.tf for example format. | <pre>list(object({<br>    rolearn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | `[]` | no |
| map\_users | Additional IAM users to add to the aws-auth configmap. | <pre>list(object({<br>    userarn  = string<br>    username = string<br>    groups   = list(string)<br>  }))</pre> | `[]` | no |
| output\_dir | Config output directory | `string` | `"./output"` | no |
| private\_subnets | List of IDs of private subnets | `list` | n/a | yes |
| tags | Project related tags | `map` | n/a | yes |
| vpc\_id | The ID of the VPC | `any` | n/a | yes |
| worker\_groups | n/a | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_iam\_role\_arn | n/a |
| cluster\_name | n/a |
| eks\_worker\_security\_group\_id | n/a |
| tags | n/a |
| users | n/a |
| worker\_iam\_role\_arn | n/a |




