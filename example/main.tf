provider "aws" {
  region = "eu-central-1"
}

data "aws_vpc" "main" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.main.id
}

locals {
  aws_region = "eu-central-1"

  project                       = "test"
  environment                   = "dev"
  env_project                   = "${local.environment}-${local.project}"
  ssh_key                       = "ssh-test"
  eks_domain_name               = "*.mindpax.me"
  eks_cluster_name              = "${local.env_project}-eks-cluster"
  eks_cluster_version           = "1.12"
  eks_cluster_config_output_dir = "./output"
  eks_cluster_tags              = { Environment = "${local.environment}" }
}

module "k8s" {
  source = "../"

  tags = merge(local.eks_cluster_tags, {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "owned"
  })

  vpc_id           = data.aws_vpc.main.id
  eks_cluster_name = local.eks_cluster_name
  cluster_version  = local.eks_cluster_version
  output_dir       = "${local.eks_cluster_config_output_dir}/"
  private_subnets  = data.aws_subnet_ids.all.ids
  efs_subnets      = data.aws_subnet_ids.all.ids

  worker_groups = [
    {
      name                 = "t2.medium-public"
      instance_type        = "t2.medium"
      asg_desired_capacity = 1
      asg_max_size         = 4
      asg_min_size         = 1
      autoscaling_enabled  = true
      kubelet_extra_args   = "--node-labels=instanceclass=on-demand,instanceenv=${local.environment}"
      termination_policies = ["OldestInstance"]
      subnets              = data.aws_subnet_ids.all.ids
      additional_userdata  = file("../scripts/lukapo_management_user_data.sh")
      key_name             = local.ssh_key
    },
  ]
}
