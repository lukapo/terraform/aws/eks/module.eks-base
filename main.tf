module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "6.0.1"

  cluster_name    = var.eks_cluster_name
  cluster_version = var.cluster_version

  subnets            = var.private_subnets
  vpc_id             = var.vpc_id
  config_output_path = "${var.output_dir}/"
  worker_groups      = var.worker_groups
  map_users          = concat(var.default_users, var.map_users)
  map_roles          = var.map_roles
  tags               = var.tags
}
