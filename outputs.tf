output "eks_worker_security_group_id" {
  value = module.eks.worker_security_group_id
}

output "cluster_iam_role_arn" {
  value = module.eks.cluster_iam_role_arn
}

output "cluster_name" {
  value = var.eks_cluster_name
}

output "worker_iam_role_arn" {
  value = module.eks.worker_iam_role_arn
}

output "users" {
  value = module.eks.workers_user_data
}

output "tags" {
  value = var.tags
}

