variable "output_dir" {
  description = "Config output directory"
  default     = "./output"
}

variable "eks_cluster_name" {
  description = "Name of EKS cluster"
  default     = "eks"
}

variable "private_subnets" {
  type        = "list"
  description = "List of IDs of private subnets"
}

variable "efs_subnets" {
  type        = "list"
  description = "List of IDs of EFS subnets"
}

variable "worker_groups" {
  type = any
}

variable "vpc_id" {
  description = "The ID of the VPC"
}

variable "tags" {
  description = "Project related tags"
  type        = "map"
}

variable "cluster_version" {
  default     = "1.12"
  description = "EKS cluster version"
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = []
}

variable "default_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = []
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap. See examples/basic/variables.tf for example format."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))
  default = []
}
